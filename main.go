package main

import (
	"fmt"
	"gosh/cli"
	"gosh/conf"
	"gosh/connections"
	"gosh/globals"
	"log"
	"os"
)

func main() {
	homePath := os.Getenv("HOME")
	configPath := fmt.Sprintf("%s/.config/%s/%s.toml", homePath, globals.ToolName, globals.ToolName)
	config, err := conf.LoadConfig(configPath)
	if err != nil {
		log.Fatal(err)
	}

	sshKeyfilePath := fmt.Sprintf("%s/.ssh/id_rsa", homePath)
	hosts, err := initHosts(config, sshKeyfilePath)
	if err != nil {
		log.Fatal(err)
	}

	defer func() {
		for _, host := range hosts {
			host.Close()
		}
	}()

	prompt := cli.NewPrompt()
	for {
		text := prompt.GetInput()
		errors := runCommands(hosts, config.Username, text)
		for _, e := range errors {
			fmt.Println(e)
		}
	}
}

func initHosts(config conf.Config, sshKeyfilePath string) ([]*connections.HostConnection, error) {
	hosts := make([]*connections.HostConnection, 0)
	for _, host := range config.Hosts {
		fmt.Printf("Connect to %s\n", host)

		hostConnection, err := connections.NewHostConnection(host, config.Username, sshKeyfilePath)
		if err != nil {
			return nil, err
		}
		hosts = append(hosts, hostConnection)
	}
	return hosts, nil
}

func runCommands(hosts []*connections.HostConnection, username, cmd string) []error {
	errors := make([]error, 0)
	for _, host := range hosts {
		fmt.Printf("Running on %s\n", host.Hostname)
		res, err := host.ExecuteCmd(cmd)
		if err != nil {
			errors = append(errors, err)
		} else {
			fmt.Print(res)
		}
	}

	return errors
}
