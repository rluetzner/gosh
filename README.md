# Gosh

Gosh is a Shell multiplexer that will send out commands via SSH to multiple hosts.

It is loosly inspired by [dsh (dancer's shell or distributed shell)](https://www.netfort.gr.jp/~dancer/software/dsh.html.en). There are multiple implementations, but none seem to be maintained any longer. Some of them also require external libraries. This is written in Go to prevent external dependencies.

## Building the tool

Clone this repository and run `go build` in the root of the checked out repository.

```bash
git clone https://github.com/rluetzner/gosh.git
cd gosh
go build
```

## How to use

### Intent

Gosh is mainly meant for easy aggregation of log files. It can be used to configure multiple hosts at the same time, but there are better tools (e.g. Ansible) for this.

### Configuration

Gosh requires a configuration file in TOML format in `$HOME/.config/gosh/gosh.toml`.

Here's an example configuration file:

```toml
username = "user"
hosts = [
        "10.10.1.1",
        "10.10.1.2",
        "10.10.1.3"
]
```
