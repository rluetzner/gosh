module gosh

go 1.17

require (
	github.com/helloyi/go-sshclient v1.1.0 // indirect
	github.com/pelletier/go-toml/v2 v2.0.0-beta.3 // indirect
	golang.org/x/crypto v0.0.0-20201221181555-eec23a3978ad // indirect
	golang.org/x/sys v0.0.0-20191026070338-33540a1f6037 // indirect
)
