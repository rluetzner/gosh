package conf

import (
	"log"
	"os"

	toml "github.com/pelletier/go-toml/v2"
)

type Config struct {
	Username string
	Hosts    []string
}

func LoadConfig(configPath string) (Config, error) {
	confData, err := os.ReadFile(configPath)
	if err != nil {
		log.Fatal(err)
	}

	var config Config
	if err := toml.Unmarshal(confData, &config); err != nil {
		return config, err
	}
	return config, nil
}
