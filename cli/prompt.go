package cli

import (
	"bufio"
	"fmt"
	"gosh/globals"
	"os"
)

type prompt struct {
	reader *bufio.Reader
}

func NewPrompt() *prompt {
	return &prompt{
		reader: bufio.NewReader(os.Stdin),
	}
}

func (p *prompt) GetInput() string {
	fmt.Printf("%s> ", globals.ToolName)
	text, _ := p.reader.ReadString('\n')
	return text
}
