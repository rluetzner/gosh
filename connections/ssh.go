package connections

import (
	"fmt"

	"github.com/helloyi/go-sshclient"
)

type HostConnection struct {
	Hostname string
	client   *sshclient.Client
}

func NewHostConnection(host, username string, sshKeyfilePath string) (*HostConnection, error) {
	client, err := sshclient.DialWithKey(fmt.Sprintf("%s:22", host), username, sshKeyfilePath)
	if err != nil {
		return nil, err
	}

	hostConnection := &HostConnection{
		Hostname: host,
		client:   client,
	}
	return hostConnection, nil
}

func (h *HostConnection) ExecuteCmd(cmd string) (string, error) {
	outBytes, err := h.client.Cmd(cmd).Output()
	if err != nil {
		return "", err
	} else {
		return fmt.Sprintf("%s", string(outBytes)), nil
	}
}

func (h *HostConnection) Close() {
	h.client.Close()
}
